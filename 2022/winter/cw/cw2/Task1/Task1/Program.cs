﻿internal class Program
{
    static void Main(string[] args)
    {
        string input = Console.ReadLine();
        string[] inputParsed = input.Split(' ');
        int[] inputInt = new int[inputParsed.Length];
        for (int i = 0; i < inputParsed.Length; i++)
        {
            inputInt[i] = int.Parse(inputParsed[i]);
        }
        rec(0, inputInt);
    }

    static bool rec(int i, int[] a)
    {
        if (i >= a.Length)
        {
            Console.WriteLine("Не существует");
            return false;
        }
        if (a[i] % 2 == 0)
        {
            Console.WriteLine("Существует");
            Console.WriteLine(a[i]);
            return true;
        }
        return rec(i + 5, a);
    }
}