﻿
static void sumator(Ratio[] arr)
{
    Ratio answer = new Ratio(0, 1);
    for (int i = 0; i < arr.Length; i++)
    {
        answer.add(arr[i]);
    }
    Console.WriteLine(answer.ToString());
}

static bool checker(Ratio[] arr)
{
    for (int i = 0; i < arr.Length; i++)
    {
        for (int j = i + 1; j < arr.Length; j++)
        {
            if (arr[i].Equals(arr[j]))
            {
                return true;
            }
        }
    }
    return false;
}