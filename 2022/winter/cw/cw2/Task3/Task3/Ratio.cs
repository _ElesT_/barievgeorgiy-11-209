﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class Ratio
{
    private int numerator;
    private int denumerator;

    public Ratio(int numerator, int denumerator)
    {
        this.numerator = numerator;
        this.denumerator = denumerator;
        if (denumerator == 0)
        {
            throw new ArgumentException("Error: denumerator equals zero.");
        }
    }

    public int Numerator { get { return numerator; } }
    public int Denumerator { get { return denumerator; } }

    public int reduce()
    {
        for (int i = Math.Min(numerator, denumerator) / 2; i > 1; i--)
        {
            if (numerator % i == 0 && denumerator % i == 0)
            {
                numerator /= i;
                denumerator /= i;
                return i;
            }
        }
        return 1;
    }

    public Ratio add(Ratio other)
    {
        int n = other.numerator, d = other.denumerator;
        n *= denumerator;
        numerator *= d;
        numerator += n;
        denumerator *= d;
        reduce();
        return this;
    }

    public Ratio mul(Ratio other)
    {
        int n = other.numerator, d = other.denumerator;
        numerator *= n;
        denumerator *= d;
        reduce();
        return this;
    }

    public override bool Equals(object? obj)
    {
        if (!(obj is Ratio))
        {
            return false;
        }
        Ratio cmp = (Ratio)obj;
        cmp.reduce();
        return cmp.Denumerator == denumerator && cmp.Numerator == numerator;
    }

    public override string ToString()
    {
        return numerator.ToString() + "/" + denumerator.ToString();
    }


}
